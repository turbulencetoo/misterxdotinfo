#!/usr/bin/python3
import http.server
import json
import mimetypes
import os
import sys

import daemon
import mrxcalc

DIR = "/home/pi/work/http"
POLLFILE = "/home/pi/work/http/poll.json"

class RequestHandler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        try:
            if self.path.startswith("/calc"):
                mrxcalc.do_GET(self)
                return

            if self.handle_special_GET():
                return

            file_path = self.path[1:]  # Remove leading '/'
            if not file_path:
                file_path = "index.html"

            content_type = mimetypes.guess_type(file_path)[0]

            if content_type and os.path.exists(file_path) and os.path.realpath(file_path).startswith(DIR):
                self.send_response(200)
                self.send_header("Content-type", content_type)
                self.end_headers()
                with open(file_path, "rb") as fp:
                    self.wfile.write(fp.read())
            else:
                print("requested file_path = {}, guessed MIME type: {}".format(file_path, content_type))
                self.send_error(404)
        except Exception as e:
            self.send_error(500, "{}".format(e))

    def do_POST(self):
        try:
            if self.path.startswith("/calc"):
                mrxcalc.do_POST(self)
                return

            if self.path != "/poll":
                self.send_error(400, "Invald post to {}".format(self.path))
                return

            self.send_response(301)
            self.send_header("Location", "http://misterx.info")
            self.end_headers()

            raw = self.rfile.read(int(self.headers['Content-Length'])).decode("utf-8")
            print("raw POST: '''{}'''".format(raw))
            vote = raw.strip().split("=")[1]  # Parse "fav=green\n" to "green" 

            with open(POLLFILE) as fp:
                results = json.load(fp)

            results[vote]["Count"] += 1
            
            with open(POLLFILE, "w") as wfp:
                json.dump(results, wfp)

        except Exception as e:
            print("{}".format(e))
            self.send_error(500, "{}".format(e))

    def handle_special_GET(self):
        '''
        Return true if a special case was handled, false if need to serve an actual file.
        '''
        cases = {"/poll-results": self.poll_results_GET,
                }
        if self.path in cases:
            cases[self.path]()
            return True
        else:
            return False

    def poll_results_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        with open(POLLFILE) as fp:
            results = json.load(fp)

        out = ["<html><title>f***poll</title>"]
        out.append("<body><ol>")
        for option in sorted(results, key=lambda x : results[x]["Count"], reverse=True):
            out.append("<li> {} ({})</li>".format(results[option]["Name"], results[option]["Count"]))
        out.append("</ol></body")
        out.append("</html>")
        self.wfile.write("".join(out).encode("utf-8"))

class WebServerDaemon(daemon.daemon):
    def __init__(self, pidfile, ip, port):
        self.server = None
        self.ip = ip
        self.port = port
        daemon.daemon.__init__(self, pidfile)

    def run(self):
        os.chdir(DIR)
        self.server = http.server.HTTPServer( (self.ip, self.port), RequestHandler)
        self.server.serve_forever()


def main():
    usage = "{0} {{start|stop|restart}}".format(sys.argv[0])
    if len(sys.argv) != 2:
        print(usage)
        sys.exit(1)
    
    pidfile = "/var/run/webserver.pid"
    ip = ""
    port = 8080
    
    serverd = WebServerDaemon(pidfile, ip, port)

    arg = sys.argv[1]
    func_map = {"start": serverd.start,
                "stop": serverd.stop,
                "restart": serverd.restart}
    
    if arg not in func_map:
        print(usage)
        sys.exit(2)

    func_map[arg]()


if __name__ == "__main__":
    main()
