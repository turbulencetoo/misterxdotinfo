# mrxcalc.py

#import traceback
import json
import re

NODETARGETSFILE = "/home/pi/work/http/node_targets.json"
CALCFILE = "/home/pi/work/http/calc.json"

# these methods extend the HTTP request handler so self will refer to that object.

def do_POST(self):
    rx = r"/calc/(\d\d?)(.*)"
    match = re.match(rx, self.path)
    turn, cmd = match.groups()

    cmds = {"move": move_POST,
            "reveal": reveal_POST,
            "nix": nix_POST,
            "reset": reset_POST}

    if cmd in cmds:
        cmds[cmd](self, turn)
    else:
        pass
        #TODO ERROR

def move_POST(self, turn):
    '''received in body as ''trans=U'' '''

    prev_turn = str(int(turn) - 1)

    raw_in = self.rfile.read(int(self.headers["Content-length"])).decode("utf-8")
    in_trans_type = raw_in.strip("trans=")

    with open(NODETARGETSFILE) as rfp:
        node_targets = json.load(rfp)

    with open(CALCFILE, "r") as rfp:
        calc_data = json.load(rfp)
    
    possible_new_nodes = set()
    possible_prev_nodes = set(calc_data[prev_turn]["Locs"]) - set(calc_data[prev_turn]["Nixed"])
    for prev_node in possible_prev_nodes:
        for trans_type in node_targets[prev_node]:
            if in_trans_type == "X" or in_trans_type == trans_type:
                possible_new_nodes.update(node_targets[prev_node][trans_type])

    calc_data[turn]["Locs"] = list(possible_new_nodes)

    with open(CALCFILE, "w") as wfp:
        json.dump(calc_data, wfp)

    self.send_response(301)
    self.send_header("Location", "http://misterx.info/calc/{}".format(turn))
    self.end_headers()
    

def reveal_POST(self, turn):
    '''received in body as ''reveal=167'' '''


    raw_in = self.rfile.read(int(self.headers["Content-length"])).decode("utf-8")
    in_loc = raw_in.strip("reveal=")

    with open(CALCFILE, "r") as rfp:
        calc_data = json.load(rfp)

    calc_data[turn]["Locs"] = [in_loc] 

    with open(CALCFILE, "w") as wfp:
        json.dump(calc_data, wfp)

    self.send_response(301)
    self.send_header("Location", "http://misterx.info/calc/{}".format(turn))
    self.end_headers()
   

def nix_POST(self, turn):

    raw_in = self.rfile.read(int(self.headers["Content-length"])).decode("utf-8")
    nodes_to_nix = [node.strip("nix=") for node in raw_in.split("&")]
    
    with open(CALCFILE) as rfp:
        calc_data = json.load(rfp)

    turn_dict = calc_data[turn]
    for node in nodes_to_nix:
        if node not in turn_dict["Nixed"]:
            turn_dict["Nixed"].append(node)

    with open(CALCFILE, "w") as wfp:
        json.dump(calc_data, wfp)

    self.send_response(301)
    self.send_header("Location", "http://misterx.info/calc/{}".format(turn))
    self.end_headers()


def reset_POST(self, turn):
    
    reset_dict= {str(i): dict(Locs=list(),Nixed=list()) for i in range(1,25)}
    with open(CALCFILE, "w") as wfp:
        json.dump(reset_dict, wfp)

    self.send_response(301)
    self.send_header("Location", "http://misterx.info/calc/2".format(turn))
    self.end_headers()


def do_GET(self):
    turn = self.path.strip("/calc/")
    next_turn = str(int(turn)+1)

    with open(CALCFILE) as rfp:
        calc_data = json.load(rfp)

    self.send_response(200)
    self.send_header("Content-type", "text/html")
    self.end_headers()

    # Hold all html output in a buffer
    out = []
    out.append("<html><head><title>f*** Turn {}</title></head>".format(turn))
    out.append("<body>")

    non_nixed_nodes = set(calc_data[turn]["Locs"]) - set(calc_data[turn]["Nixed"])
    if non_nixed_nodes:
        # List possible nodes and allow to be nixed
        out.append('<form action="http://misterx.info/calc/{}nix" method="post">'.format(turn))
        out.append('<p>Possible nodes for Mr. X on turn {}:<br>'.format(turn))
        for node in sorted(non_nixed_nodes, key=lambda x : int(x)):
            out.append('<input name="nix" type="checkbox" value="{}">"{}"<br>'.format(node,node))
        out.append('<input type="submit" value="Nix These Nodes">')
        out.append('</p></form>')
        out.append('<br>')

    if next_turn not in ['3','8','13','18']:
        # List options for mister X next move
        out.append('<form action="http://misterx.info/calc/{}move" method="post">'.format(next_turn))
        out.append('<p> Which mode of transport did Mr. X use on turn {}?<br>'.format(next_turn))
        for key_letter, trans_name in [("T", "Taxi"), ("B", "Bus"), ("U", "Underground"), ("X", "Misdirect")]:
            out.append('<input name="trans" type="radio" value={}>{}<br>'.format(key_letter,trans_name))
        out.append('<input type="submit" value="Mr. X is on the move!">')
        out.append('</p></form>')
        out.append('<br>')
    else:
        # Let enter where he revealed on next move
        out.append('<form action="http://misterx.info/calc/{}reveal" method="post">'.format(next_turn))
        out.append('<p> Mr. X revealed on turn {}, where is he?<br>'.format(next_turn))
        out.append('<input name="reveal" type="text"><br>')
        out.append('<input type="submit" value="We see you Mr. X!">')
        out.append('</p></form>')
        out.append('<br>')

    # Option to reset everything and go back before first reveal
    out.append('<br><br><br>')
    out.append('<form action="http://misterx.info/calc/0reset" method="post"><p>')
    out.append('<button type="submit">Reset All!</button><br>')
    out.append('</p></form>')

    out.append("</body></html>") 

    # Ship it!
    self.wfile.write(''.join(out).encode('utf-8'))

